<?php /** @noinspection ALL */

declare(strict_types=1);

namespace AppBundle\Payout\Splitter;

use AppBundle\Entity\Finance\Payout\Payout;
use AppBundle\Entity\Finance\Payout\PayoutGroup as PayoutGroupEntity;
use AppBundle\Payout\Calculator\TotalAmountInterface;
use Doctrine\Common\Collections\ArrayCollection;

final class PayoutGroup implements SplitterInterface
{
    /**
     * @var SplitterInterface
     */
    private $innerSplitter;

    /**
     * @var TotalAmountInterface
     */
    private $totalAmountCalculator;

    public function __construct(SplitterInterface $innerSplitter, TotalAmountInterface $totalAmountCalculator)
    {
        $this->innerSplitter = $innerSplitter;
        $this->totalAmountCalculator = $totalAmountCalculator;
    }

    public function split(Payout $payout): array
    {
        $payoutList = $this->innerSplitter->split($payout);

        if (count($payoutList) > 1) {
            $payoutGroup = new PayoutGroupEntity();

            $payoutGroup->setPayouts(new ArrayCollection($payoutList));
            $payoutGroup->setAmountMoney(
                $this->totalAmountCalculator->calculate($payoutList)
            );

            foreach ($payoutList as $item) {
                $item->setPayoutGroup($payoutGroup);
            }
        }

        return $payoutList;
    }
}
