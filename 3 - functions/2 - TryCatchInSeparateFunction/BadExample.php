<?php /** @noinspection ALL */

namespace AppBundle\SmsCampaign\Service;

use AppBundle\Entity\SmsCampaign\Schedule;
use AppBundle\Entity\SmsCampaign\ScheduleSubscriber;
use AppBundle\SmsCampaign\Dto\Message;
use AppBundle\SmsCampaign\Repository\ScheduleRepositoryInterface;
use AppBundle\SmsCampaign\Repository\ScheduleSubscriberRepositoryInterface;
use AppBundle\SmsCampaign\Repository\SendingLogRepositoryInterface;

final class MessageHandler implements MessageHandlerInterface
{
    /**
     * @var ScheduleRepositoryInterface
     */
    private $scheduleRepository;

    /**
     * @var ScheduleSubscriberRepositoryInterface
     */
    private $scheduleSubscriberRepository;

    /**
     * @var SendingLogRepositoryInterface
     */
    private $sendingLogRepository;

    /**
     * @var SmsCampaignPublisherInterface
     */
    private $publisher;

    public function __construct(
        ScheduleRepositoryInterface $scheduleRepository,
        ScheduleSubscriberRepositoryInterface $scheduleSubscriberRepository,
        SendingLogRepositoryInterface $sendingLogRepository,
        SmsCampaignPublisherInterface $publisher
    ) {
        $this->scheduleRepository = $scheduleRepository;
        $this->scheduleSubscriberRepository = $scheduleSubscriberRepository;
        $this->sendingLogRepository = $sendingLogRepository;
        $this->publisher = $publisher;
    }
    public function enqueueMessages(): void
    {
        $schedules = $this->scheduleRepository->getWaitingSchedules();

        foreach ($schedules as $schedule) {
            $subscribers =
                $this->scheduleSubscriberRepository->findSubscribersByScheduleIdAndStatus(
                    $schedule->getId(),
                    ScheduleSubscriber::SENDING_STATUS_NEW
                );

            $subscriberIds = [];
            $failedSubscriberIds = [];
            foreach ($subscribers as $subscriber) {
                if (empty($subscriber['phone'])) {
                    continue;
                }

                $message = (new Message())
                    ->setPhone($subscriber['phone'])
                    ->setText($schedule->getContent())
                    ->setScheduleId($schedule->getId())
                    ->setSubscriberId($subscriber['subscriber']->getId());

                try {
                    $this->publisher->publish($message);
                } catch (\Throwable $e) {
                    $this->sendingLogRepository->log($subscriber['subscriber'], $e->getMessage());

                    $failedSubscriberIds[] = $subscriber['subscriber']->getId();

                    continue;
                }

                $subscriberIds[] = $subscriber['subscriber']->getId();
            }

            $this->scheduleSubscriberRepository->updateSubscribersByIds(
                $subscriberIds,
                ['status' => ScheduleSubscriber::SENDING_STATUS_QUEUED]
            );

            $this->scheduleSubscriberRepository->updateSubscribersByIds(
                $failedSubscriberIds,
                ['status' => ScheduleSubscriber::SENDING_STATUS_ERROR]
            );

            $this->scheduleRepository->updateSchedule(
                $schedule->getId(),
                ['status' => Schedule::STATUS_IN_PROGRESS]
            );
        }
    }
}
