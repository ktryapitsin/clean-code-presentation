<?php /** @noinspection ALL */

namespace AppBundle\CommandBus2\Worker;

use AppBundle\CommandBus2\MayBeQueuedInterface;
use AppBundle\CommandBus2\Worker\Exception\MessageBusHandleException;
use AppBundle\CommandQueue\Queue\CommandQueueInterface;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use SimpleBus\Message\Bus\MessageBus;
use Throwable;

class CommandBusWorker implements CommandBusWorkerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CommandQueueInterface
     */
    private $queue;

    /**
     * @var MessageBus
     */
    private $messageBus;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $process_id;

    public function __construct(
        EntityManager $em,
        CommandQueueInterface $queue,
        MessageBus $messageBus,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->queue = $queue;
        $this->messageBus = $messageBus;
        $this->logger = $logger;
        $this->process_id = $this->getWorkerId();
    }

    /**
     * @param int[] $queueIds
     *
     * @return bool
     *
     * @throws Throwable
     */
    public function processMessage(array $queueIds): bool
    {
        $this->logger->debug('CommandBusWorker: start process');
        $message = $this->queue->consume($this->process_id, $queueIds);
        if (null === $message) {
            return false;
        }
        $this->logger->debug('CommandBusWorker: proces message', ['message_id' => $message->getId()]);
        try {
            $this->em->beginTransaction();

            $this->queue->markMessageAsInProgress($message, $this->process_id);

            $command = $message->getCommand();

            if ($command instanceof MayBeQueuedInterface) {
                $command->setAllowedToQueue(false);
                $this->logger->debug('CommandBusWorker: setAllowedToQueue(false)');
            }

            $this->logger->debug('CommandBusWorker: start handle command');
            $this->handle($command);
            $this->logger->debug('CommandBusWorker: end handle command');

            $this->queue->markMessageAsDone($message, $this->process_id);

            $this->em->flush();
            $this->em->commit();
            $this->em->clear();
        } catch (Throwable $e) {
            $this->em->close();
            $this->em->rollback();
            $this->queue->markMessageAsFailed($message, $this->process_id);
            throw $e;
        }
        $this->logger->debug('CommandBusWorker: end process');

        return true;
    }

    private function getWorkerId(): string
    {
        return sprintf(
            '%s::%s::%s',
            gethostname(),
            getmypid(),
            time()
        );
    }
    /**
     * @param $command
     * @throws MessageBusHandleException
     */
    private function handle($command): void
    {
        try {
            $this->messageBus->handle($command);
        } catch (Throwable $exception) {
            throw new MessageBusHandleException('Cannot handle command', 0, $exception);
        }
    }
}
