<?php /** @noinspection ALL */

declare(strict_types=1);

class Toto
{
    private function calculateDrawingOutcome(TotoDrawing $drawing): int
    {
        if ($drawing->getClosedEventsCount() !== 15) {
            throw new \LogicException('All events must be closed prior to calculating the result of drawing!');
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        $eventBitmaps = [];
        foreach ($drawing->getEvents() as $event) {
            $eventBitmaps[$event->getPosition()] = 0;
            switch ($event->getStatus()) {
                case TotoDrawingEvent::STATUS__HAPPENED:
                    foreach (['2', 'X', '1'] as $outcomeBitPosition => $outcomeName) {
                        /** @var LineOutcome $outcome */
                        $outcome = $accessor->getValue($event, 'outcome' . $outcomeName);

                        if ($outcome->getStatus() === LineOutcome::STATUS_WIN) {
                            $eventBitmaps[$event->getPosition()] |= 1 << $outcomeBitPosition;
                        }
                    }

                    break;
                case TotoDrawingEvent::STATUS__NOT_HAPPENED:
                    // в случае несостоявшегося события "играют" все варианты
                    $eventBitmaps[$event->getPosition()] = 0b111;

                    break;
            }
        }

        $result = 0;
        foreach ($eventBitmaps as $bitmapPosition => $bitmap) {
            $result |= $bitmap << (3 * ($bitmapPosition - 1));
        }
        return $result;
    }
}