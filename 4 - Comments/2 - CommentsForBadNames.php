<?php /** @noinspection ALL */

namespace AppBundle\Casino\Slotegrator;

use AppBundle\Coupon\Service\CouponCountStorage\CouponCountStorageInterface;
use AppBundle\Coupon\Service\CouponCountStorage\Exception\CouponCountStorageException;
use AppBundle\DataAccess\TransactionDataAccess;
use AppBundle\DataAccess\TransactionDataAccessInterface;
use AppBundle\Entity\AbstractCoupon;
use AppBundle\Entity\CasinoGame;
use AppBundle\Entity\Coupon;
use AppBundle\Entity\SlotegratorCoupon;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Finance\Currency;
use AppBundle\Finance\Exception\InsufficientBalanceException;
use AppBundle\Finance\FinanceManager;
use AppBundle\Finance\Money;
use Doctrine\DBAL\TransactionIsolationLevel;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\PessimisticLockException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Slotegrator
{
    private function bet(
        $amount,                // Bet amount
        $currency,              // Bet currency
        $gameUuid,              // Game UUID from the list of games  /games
        $playerId,              // Unique player ID on integrator side
        $transactionId,         // Unique transaction ID on GIS side
        $sessionId,             // Integrator game session ID, provided in  /games/init
        $type           = 'bet' // "bet" or "tip" // we do not expect "tip" requests to come
    ) {
        $user = $this->requireUser($playerId);
        $game = $this->requireGame($gameUuid);

        $couponRepository = $this->em->getRepository(SlotegratorCoupon::class);
        $coupon = $couponRepository->findByBetTransactionId($transactionId);
        if (!$coupon instanceof SlotegratorCoupon) {
            $coupon = new SlotegratorCoupon(
                $user,
                $game,
                $sessionId,
                $transactionId,
                Money::create(Currency::createByCode($currency), $amount)
            );

            $this->em->getConnection()->setTransactionIsolation(TransactionIsolationLevel::READ_COMMITTED);
            $this->em->transactional(
                function () use ($coupon) {
                    $balance = $this->fm->getBalance(
                        $coupon->getUser(),
                        Transaction::ACCOUNT_CHECKING,
                        null,
                        true
                    );

                    if ($coupon->getAmountMoney()->greaterThan($balance)) {
                        throw new InsufficientBalanceException('');
                    }

                    $this->em->persist($coupon);
                    $this->em->flush($coupon);

                    $this->fm->makeCouponWithdraw($coupon);
                }
            );

            $this->incrementCasinoGameCouponCount($coupon->getGame());
        }

        return $this->createResponse($user);
    }

    private function win(
        $amount,        // Win amount
        $currency,      // Win currency
        $gameUuid,      // Game UUID from the list of games  /games
        $playerId,      // Unique player ID on integrator side
        $transactionId, // Unique transaction ID on GIS side
        $sessionId,     // Integrator game session ID, provided in  /games/init
        $type           // "win" or "jackpot"
    ) {
        $user = $this->requireUser($playerId);
        $game = $this->requireGame($gameUuid);

        $couponRepository = $this->em->getRepository(SlotegratorCoupon::class);
        if ($couponRepository->isAlreadyClosed($sessionId, $transactionId)) {
            return $this->createResponse($user);
        }

        // lock to prevent double processing
        $lockName = sprintf('slotegrator-%d-%s', $user->getId(), $sessionId);
        $locked = $this->em->getConnection()->executeQuery(
            'SELECT GET_LOCK(?, 2)',
            [$lockName]
        )->fetchColumn();
        if ('1' !== $locked) {
            throw new PessimisticLockException(sprintf('Unable to get coupon lock, user: #%d', $user->getId()));
        }

        $coupon = $couponRepository->findForClosing($sessionId);
        if (!$coupon instanceof SlotegratorCoupon) {
            // This is free (bonus) game
            $coupon = new SlotegratorCoupon(
                $user,
                $game,
                $sessionId,
                $transactionId,
                Money::create(Currency::createByCode($currency), 0)
            );

            $this->em->transactional(
                function () use ($coupon) {
                    $this->em->persist($coupon);
                    $this->em->flush($coupon);
                }
            );

            $this->incrementCasinoGameCouponCount($coupon->getGame());
        }

        $winMoney = Money::create(Currency::createByCode($currency), $amount);
        $this->em->transactional(
            function () use ($coupon, $winMoney, $transactionId, $currency, $type) {
                if ($type === 'jackpot') {
                    $coupon->setWinAmount($winMoney->getValue());
                } else {
                    $couponWinAmountMoney = $coupon->getWinAmount() !== null
                        ? $coupon->getWinAmountMoney()
                        : Money::create(Currency::createByCode($currency), 0);

                    $couponWinAmountMoneyFinal = $couponWinAmountMoney->add($winMoney);
                    $coupon->setWinAmount($couponWinAmountMoneyFinal->getValue());

                    if ($couponWinAmountMoneyFinal->greaterThanZero()) {
                        $coupon->setStatus(Coupon::STATUS_WIN);
                        $this->fm->makeCouponWin($coupon);
                    } else {
                        $coupon->setStatus(Coupon::STATUS_LOSE);
                        $this->fm->makeCouponLose($coupon);
                    }
                }

                $coupon->setWinTransactionId($transactionId);

                $this->em->flush($coupon);
            }
        );

        // release lock after coupon has closed
        $this->em->getConnection()->executeQuery(
            'SELECT RELEASE_LOCK(?)',
            [$lockName]
        );

        return $this->createResponse($user);
    }

    private function refund(
        $amount,              // Refund amount
        $currency,            // Refund currency
        $playerId,      // Unique player ID on integrator side
        $refundTransactionId, // Unique transaction ID on GIS side
        $betTransactionId     // GIS bet transaction ID to be refunded
    ) {
        $user = $this->requireUser($playerId);

        $couponRepository = $this->em->getRepository(SlotegratorCoupon::class);
        $coupon = $couponRepository->findByBetTransactionId($betTransactionId);
        if ($coupon instanceof SlotegratorCoupon && !$coupon->isClosed()) {
            $this->validateRefundMoney($coupon, $amount, $currency);

            $this->em->transactional(
                function () use ($coupon, $refundTransactionId) {
                    $this->fm->makeCouponCancel($coupon);
                    $coupon->setStatus(AbstractCoupon::STATUS_CANCEL);

                    $coupon->setWinTransactionId($refundTransactionId);

                    $this->em->flush($coupon);
                }
            );
        }

        return $this->createResponse($user);
    }
}
