<?php /** @noinspection ALL */

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup
// for more information
//umask(0000);

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__ . '/../app/autoload.php';
Debug::enable();

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);
$request = Request::createFromGlobals();
if (!$request->server->has('HTTP_X_REQUEST_ID')) {
    $request->server->set('HTTP_X_REQUEST_ID', uniqid('', true));
}
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
