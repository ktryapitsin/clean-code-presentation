

// Вспомогательный метод; возвращает управление, когда значение this.closed истинно.
// Инициирует исключение при достижении тайм-аута.
public synchronized void waitForClose(final long timeoutMillis) throws Exception {
    if(!closed) {
      wait(timeoutMillis);
      if(!closed)
          throw new Exception("MockResponseSender could not be closed");
    }
}