<?php /** @noinspection ALL */

namespace TestingBundle\Controller\Finance;

use AppBundle\AppEvents;
use AppBundle\Entity\Finance\Refill;
use AppBundle\Entity\User;
use AppBundle\Finance\Event\RefillSucceeded;
use AppBundle\Finance\FinanceManagerInterface;
use AppBundle\Finance\Money;
use AppBundle\Model\PaymentDetails\YandexMoney;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TestingBundle\Repository\Exception\NotFoundException;
use TestingBundle\Repository\User\UserCriteria;
use TestingBundle\Repository\User\UserRepositoryInterface;

final class CreateRefill
{
    /**
     * @param Request $request
     * @return User
     * @throws NotFoundException
     */
    private function obtainUser(Request $request): User
    {
        return
            $this->userRepository
                ->obtainUser(
                    (new UserCriteria())
                        ->setEmail($request->get('email'))
                        ->setPhone($request->get('phone'))
                        ->setUserId($request->get('user_id'))
                );
    }
}

/**
 * PaymentMethod
 *
 * @ORM\Table(name="payment_method")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentMethodRepository")
 */
class PaymentMethod
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PaymentMethod
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PaymentMethod
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return PaymentMethod
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
