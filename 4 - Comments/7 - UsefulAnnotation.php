<?php /** @noinspection ALL */

use Tests\AbstractTestCase;

declare(strict_types=1);

final class SubCouponWinAmountEqualToCouponWinAmountTest extends AbstractTestCase
{
    /**
     * @test
     * @expectedException \AppRuBundle\Finance\FinanceManager\PostProcessor\SubAccount\Context\Exception\ContextValidateException
     * @expectedExceptionMessage Coupon amount is not equal to Sub coupon sum amount
     */
    public function validateExceptional(): void
    {
        $this->setUpConfig(self::COUPON_AMOUNT_EXCEPTIONAL);
        $validator = $this->createValidator();

        $validator->validate($this->createContext());
    }
}
