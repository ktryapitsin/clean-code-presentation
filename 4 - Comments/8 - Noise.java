


class AnnualDateRule
{
    /**
     * Конструктор по умолчанию.
     */
    protected AnnualDateRule() {

    }

    /** День месяца. */
    private int dayOfMonth;

    /**
    * Возвращает день месяца.
    *
    * @return день месяца.
    */
    public int getDayOfMonth() {
        return dayOfMonth;
    }
}