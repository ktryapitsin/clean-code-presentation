<?php /** @noinspection ALL */

namespace AppBundle\Command;

use AppBundle\CommandBus2\Command\RecalcCoupons;
use AppBundle\Entity\Coupon;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MostbetRecalcCouponCommand extends ContainerAwareCommand
{
    /**
     * @param int[]           $ids
     * @param OutputInterface $output
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function processCouponIdChunk(array $ids, OutputInterface $output)
    {
        $commandBus = $this->getContainer()->get('command_bus');

        $command = new RecalcCoupons();
        $command->setAllowedToQueue(false);
        $command->couponIds = $ids;

        $this->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->beginTransaction();

        try {
            $commandBus->handle($command);

            $this->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->commit();

            $output->writeln('Done');
        } catch (\Throwable $e) {
            $this->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->rollBack();
            $output->writeln('Error: ' . $e->getMessage());
        }
    }
}
