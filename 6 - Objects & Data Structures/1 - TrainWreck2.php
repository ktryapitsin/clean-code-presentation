<?php /** @noinspection ALL */

namespace AppBundle\Command;

use AppBundle\CommandBus2\Command\RecalcCoupons;
use AppBundle\Entity\Coupon;
use Doctrine\ORM\EntityManager;
use PDO;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MostbetRecalcCouponCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var MessageBus
     */
    private $commandBus;

    public function __construct(EntityManager $em, MessageBus $commandBus)
    {
        $this->entityManager = $em;
        $this->commandBus = $commandBus;
    }

    /**
     * @param int[]           $ids
     * @param OutputInterface $output
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function processCouponIdChunk(array $ids, OutputInterface $output)
    {
        $command = new RecalcCoupons();
        $command->setAllowedToQueue(false);
        $command->couponIds = $ids;

        $this->entityManager->getConnection()->beginTransaction();

        try {
            $this->commandBus->handle($command);

            $this->entityManager->getConnection()->commit();

            $output->writeln('Done');
        } catch (\Throwable $e) {
            $this->entityManager->getConnection()->rollBack();
            $output->writeln('Error: ' . $e->getMessage());
        }
    }
}
