<?php /** @noinspection ALL */

namespace AppBundle\Command;

use AppBundle\CommandBus2\Command\RecalcCoupons;
use AppBundle\Entity\Coupon;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MostbetRecalcCouponCommand extends ContainerAwareCommand
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var MessageBus
     */
    private $commandBus;

    public function __construct(Connection $dbalConnection, MessageBus $commandBus)
    {
        $this->connection = $dbalConnection;
        $this->commandBus = $commandBus;
    }

    /**
     * @param int[]           $ids
     * @param OutputInterface $output
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function processCouponIdChunk(array $ids, OutputInterface $output)
    {
        $command = new RecalcCoupons();
        $command->setAllowedToQueue(false);
        $command->couponIds = $ids;

        $this->connection->beginTransaction();

        try {
            $this->commandBus->handle($command);

            $this->connection->commit();

            $output->writeln('Done');
        } catch (\Throwable $e) {
            $this->connection->rollBack();
            $output->writeln('Error: ' . $e->getMessage());
        }
    }
}
