public class DeviceController {
    // ...
    public void sendShutDown() {
        DeviceHandle handle = getHandle(DEV1);
        // Проверить состояние устройства
        if (handle != DeviceHandle.INVALID) {
            // Сохранить состояние устройства в поле записи
            retrieveDeviceRecord(handle);
            // Если устройство не приостановлено, отключить его
            if (record.getStatus() != DEVICE_SUSPENDED) {
                pauseDevice(handle);
                clearDeviceWorkQueue(handle);
                closeDevice(handle);
            } else {
                logger.log("Device suspended.  Unable to shut down");
            }
        } else {
            logger.log("Invalid handle for: " + DEV1.toString());
        }
    }
    // ...
}